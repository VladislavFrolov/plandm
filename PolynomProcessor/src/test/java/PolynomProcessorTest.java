import org.junit.Assert;
import org.junit.Test;

public class PolynomProcessorTest {
    @Test
    public void maxroot1() {

        try {
            Polynom a = new Polynom(0, 0, 0);
            PolynomProcessor aa = new PolynomProcessor(a);
            double etalon = 0;
            double e = aa.getMaxRoot();
            Assert.assertEquals(e, etalon, 0);

        } catch (PolynomException e) {
            e.printStackTrace();
        } catch (PolynomSolveException e) {
            e.printStackTrace();
        }

    }

    @Test(expected = PolynomSolveException.class)
    public void maxroot2() throws PolynomException, PolynomSolveException {


        Polynom a = new Polynom(1, 2, 3);
        PolynomProcessor aa = new PolynomProcessor(a);
        double etalon = 0;
        double e = aa.getMaxRoot();
        Assert.assertEquals(e, etalon, 0);

    }

    @Test
    public void maxroot3() {

        try {
            Polynom a = new Polynom(1, 3, -4);
            PolynomProcessor aa = new PolynomProcessor(a);
            double etalon = 1;
            double e = aa.getMaxRoot();
            Assert.assertEquals(e, etalon, 0);

        } catch (PolynomException e) {
            e.printStackTrace();
        } catch (PolynomSolveException e) {
            e.printStackTrace();
        }

    }
}