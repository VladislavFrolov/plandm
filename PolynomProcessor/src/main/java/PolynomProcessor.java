public class PolynomProcessor {
    private Polynom polynom;

    public PolynomProcessor(Polynom polynom) {
        this.polynom = polynom;
    }

    public double getMaxRoot() throws PolynomSolveException, PolynomException {

        if (polynom.getA()==0) {
        throw new PolynomException("Invalid parameters");
        } else {
            return (polynom.determination()[0] > polynom.determination()[1]) ? polynom.determination()[0] : polynom.determination()[1];
        }

    }
}

