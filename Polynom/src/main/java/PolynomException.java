
public class PolynomException extends Exception {

    public PolynomException(String message) {        super(message);    }

    public PolynomException(String message, Throwable cause) {
        super(message, cause);
    }

    public PolynomException(Throwable cause) { super(cause);    }
}
