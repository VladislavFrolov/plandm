
public class Polynom {private double a;
    private double b;
    private double c;
    private double constanta;
    public static final double eps= 10^(-7);

    public Polynom(double a, double b, double c) throws PolynomException {
        if (Math.abs(a) < eps
        ) {
            throw new PolynomException("Invalid parameters");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double[]  determination() throws PolynomSolveException {
        double[] roots;
        if (a != 0) {
            double discriminat = b * b - 4 * a * c;
            if (discriminat < 0) {
                throw new PolynomSolveException("Roots aren't Real");
            }
            if (Math.abs(b) < eps&&Math.abs(c)<eps) {
                roots = new double[2];
                roots[0] = 0;
                roots[1] = 0;
                return roots;
            }
            roots = new double[2];
            roots[0] = (-b+Math.sqrt(discriminat))/(a*2);
            roots[1] = (-b-Math.sqrt(discriminat))/(a*2);


System.out.println("Корни подсчитаны");
        }
        else {
            roots = new double[1];
            roots[0] = -c / b;
        }
        return roots;
    }

    public static void main(String[] args) {
      try {double[] solve;
          Polynom x = new Polynom(-1, 0, 1);
          solve=x.determination();
          for (int i=0;i<solve.length;i++){
              System.out.println(solve[i]);

          }
      }
       catch (PolynomException e ){System.out.println(e);}
       catch (PolynomSolveException r){System.out.println(r);}
    }

}
