
public class PolynomSolveException extends Exception {

    public PolynomSolveException(String message) {
        super(message);
    }

    public PolynomSolveException(String message, Throwable cause) {
        super(message, cause);
    }

    public PolynomSolveException(Throwable cause) {
        super(cause);
    }
}


