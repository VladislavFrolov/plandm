
import org.junit.Test;
import org.junit.Assert;
import java.util.Arrays;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class PolynomTest {
    @Test
    public void testContructor1() {
        try {
            Polynom a = new Polynom(1, 2, 3);
        } catch (PolynomException e) {}
    }
    @Test
    public void testContructor2() throws PolynomException {
        try {
            Polynom a = new Polynom(0, 0, 3);
        }catch (PolynomException e) {}
    }

    @Test
    public void testDetermination1(){

        try {Polynom a= new Polynom(1 ,-5,4 );
            double[] etalon= new double[2];
            etalon[0]=4;
            etalon[1]=1;
            double[] e=a.determination();
            assertTrue(Arrays.equals(etalon, e));

        }catch (PolynomException e) {}
        catch (PolynomSolveException e){}



    }
    @Test
    public void testDetermination2(){
        try {Polynom a= new Polynom(1 ,0,-4 );
            double[] etalon= new double[2];
            etalon[0]=2;
            etalon[1]=-2;
            double[] e=a.determination();
            assertTrue(Arrays.equals(etalon, e));

        }catch (PolynomException e) {}
        catch (PolynomSolveException e){}



    }
    @Test
    public void testDetermination3(){
        try {Polynom a= new Polynom(1 ,0,0 );
            double[] etalon= new double[2];
            etalon[0]=0.0;
            etalon[1]=0.0/(-1.0);
            double[] e=a.determination();
            assertTrue(Arrays.equals(etalon, e));

        }catch (PolynomException e) {}
        catch (PolynomSolveException e){}



    }
    @Test(expected = PolynomSolveException.class)
    public void testDetermination4()throws PolynomSolveException{
        try {Polynom a= new Polynom(1 ,2,4 );
            double[] etalon= new double[2];
            etalon[0]=2;
            etalon[1]=-2;
            double[] e=a.determination();
            assertTrue(Arrays.equals(etalon, e));

        }catch (PolynomException e) {}



    }


}

